
# on Readme.md
1. https://github.com/matiassingers/awesome-readme "A curated list of awesome READMEs"
1.  https://www.makeareadme.com/ "Make a README. Because no one can read your mind (yet)"

# Example Profile Readme
1. https://github.com/jlengstorf
1. https://github.com/cyrisxd
1. https://github.com/m0nica
    1. https://www.aboutmonica.com/projects/#author-bio
1. https://github.com/katmeister
    1. https://dev.to/web/design-github-profile-using-readme-md-8al
1. https://twitter.com/sudo_overflow/status/1281146411736694784
1. https://github.com/argyleink

# Collection of Profile Readme files
1. https://github.com/kautukkundan/Awesome-Profile-README-templates

# Tips & Tricks on Profile Readme files
1. https://www.mokkapps.de/blog/how-i-built-a-self-updating-readme-on-my-git-hub-profile/
1. https://eugeneyan.com/writing/how-to-update-github-profile-readme-automatically/
